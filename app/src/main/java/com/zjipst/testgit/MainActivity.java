package com.zjipst.testgit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

/**
 * @author duxiaoming
 * @email 957708598@qq.com
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
